# E-Vehicle-Rent-MONOLITHIC

Monolithic app for electrical vehicle rent system

## Getting started

To run this project, you will need some prerequisites, such as :

* Java 11
* Maven
* PostgreSQL
* IDE

## Starting The Application

1. Make sure all the properties match with your current system credentials. Such as database username & password.

2. You need to install all the included dependencies in the project by using Maven, with the following commands :

```
mvn clean install
```

4. After all the prerequisites are already completed, you can run this application smoothly.
```
java -jar
```
