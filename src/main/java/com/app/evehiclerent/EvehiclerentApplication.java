package com.app.evehiclerent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvehiclerentApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvehiclerentApplication.class, args);
	}

}
