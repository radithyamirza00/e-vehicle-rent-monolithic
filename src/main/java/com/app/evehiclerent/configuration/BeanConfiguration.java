package com.app.evehiclerent.configuration;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.MappingStrategy;
import ma.glasnost.orika.impl.MapperFacadeImpl;
import ma.glasnost.orika.metadata.Type;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Configuration
public class BeanConfiguration {

    @Bean
    public MapperFacade mapperFacade(){
        return new MapperFacade() {
            @Override
            public <S, D> D map(S s, Class<D> aClass) {
                return null;
            }

            @Override
            public <S, D> D map(S s, Class<D> aClass, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> void map(S s, D d) {

            }

            @Override
            public <S, D> void map(S s, D d, MappingContext mappingContext) {

            }

            @Override
            public <S, D> void map(S s, D d, Type<S> type, Type<D> type1) {

            }

            @Override
            public <S, D> void map(S s, D d, Type<S> type, Type<D> type1, MappingContext mappingContext) {

            }

            @Override
            public <S, D> Set<D> mapAsSet(Iterable<S> iterable, Class<D> aClass) {
                return null;
            }

            @Override
            public <S, D> Set<D> mapAsSet(Iterable<S> iterable, Class<D> aClass, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> Set<D> mapAsSet(S[] s, Class<D> aClass) {
                return null;
            }

            @Override
            public <S, D> Set<D> mapAsSet(S[] s, Class<D> aClass, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> List<D> mapAsList(Iterable<S> iterable, Class<D> aClass) {
                return null;
            }

            @Override
            public <S, D> List<D> mapAsList(Iterable<S> iterable, Class<D> aClass, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> List<D> mapAsList(S[] s, Class<D> aClass) {
                return null;
            }

            @Override
            public <S, D> List<D> mapAsList(S[] s, Class<D> aClass, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> D[] mapAsArray(D[] ds, Iterable<S> iterable, Class<D> aClass) {
                return null;
            }

            @Override
            public <S, D> D[] mapAsArray(D[] ds, S[] s, Class<D> aClass) {
                return null;
            }

            @Override
            public <S, D> D[] mapAsArray(D[] ds, Iterable<S> iterable, Class<D> aClass, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> D[] mapAsArray(D[] ds, S[] s, Class<D> aClass, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> void mapAsCollection(Iterable<S> iterable, Collection<D> collection, Class<D> aClass) {

            }

            @Override
            public <S, D> void mapAsCollection(Iterable<S> iterable, Collection<D> collection, Class<D> aClass, MappingContext mappingContext) {

            }

            @Override
            public <S, D> void mapAsCollection(S[] s, Collection<D> collection, Class<D> aClass) {

            }

            @Override
            public <S, D> void mapAsCollection(S[] s, Collection<D> collection, Class<D> aClass, MappingContext mappingContext) {

            }

            @Override
            public <S, D> D map(S s, Type<S> type, Type<D> type1) {
                return null;
            }

            @Override
            public <S, D> D map(S s, Type<S> type, Type<D> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> Set<D> mapAsSet(Iterable<S> iterable, Type<S> type, Type<D> type1) {
                return null;
            }

            @Override
            public <S, D> Set<D> mapAsSet(Iterable<S> iterable, Type<S> type, Type<D> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> Set<D> mapAsSet(S[] s, Type<S> type, Type<D> type1) {
                return null;
            }

            @Override
            public <S, D> Set<D> mapAsSet(S[] s, Type<S> type, Type<D> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> List<D> mapAsList(Iterable<S> iterable, Type<S> type, Type<D> type1) {
                return null;
            }

            @Override
            public <S, D> List<D> mapAsList(Iterable<S> iterable, Type<S> type, Type<D> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> List<D> mapAsList(S[] s, Type<S> type, Type<D> type1) {
                return null;
            }

            @Override
            public <S, D> List<D> mapAsList(S[] s, Type<S> type, Type<D> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> D[] mapAsArray(D[] ds, Iterable<S> iterable, Type<S> type, Type<D> type1) {
                return null;
            }

            @Override
            public <S, D> D[] mapAsArray(D[] ds, S[] s, Type<S> type, Type<D> type1) {
                return null;
            }

            @Override
            public <S, D> D[] mapAsArray(D[] ds, Iterable<S> iterable, Type<S> type, Type<D> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> D[] mapAsArray(D[] ds, S[] s, Type<S> type, Type<D> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> void mapAsCollection(Iterable<S> iterable, Collection<D> collection, Type<S> type, Type<D> type1) {

            }

            @Override
            public <S, D> void mapAsCollection(Iterable<S> iterable, Collection<D> collection, Type<S> type, Type<D> type1, MappingContext mappingContext) {

            }

            @Override
            public <S, D> void mapAsCollection(S[] s, Collection<D> collection, Type<S> type, Type<D> type1) {

            }

            @Override
            public <S, D> void mapAsCollection(S[] s, Collection<D> collection, Type<S> type, Type<D> type1, MappingContext mappingContext) {

            }

            @Override
            public <S, D> D convert(S s, Class<D> aClass, String s1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> D convert(S s, Type<S> type, Type<D> type1, String s1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <Sk, Sv, Dk, Dv> Map<Dk, Dv> mapAsMap(Map<Sk, Sv> map, Type<? extends Map<Sk, Sv>> type, Type<? extends Map<Dk, Dv>> type1) {
                return null;
            }

            @Override
            public <Sk, Sv, Dk, Dv> Map<Dk, Dv> mapAsMap(Map<Sk, Sv> map, Type<? extends Map<Sk, Sv>> type, Type<? extends Map<Dk, Dv>> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, Dk, Dv> Map<Dk, Dv> mapAsMap(Iterable<S> iterable, Type<S> type, Type<? extends Map<Dk, Dv>> type1) {
                return null;
            }

            @Override
            public <S, Dk, Dv> Map<Dk, Dv> mapAsMap(Iterable<S> iterable, Type<S> type, Type<? extends Map<Dk, Dv>> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, Dk, Dv> Map<Dk, Dv> mapAsMap(S[] s, Type<S> type, Type<? extends Map<Dk, Dv>> type1) {
                return null;
            }

            @Override
            public <S, Dk, Dv> Map<Dk, Dv> mapAsMap(S[] s, Type<S> type, Type<? extends Map<Dk, Dv>> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <Sk, Sv, D> List<D> mapAsList(Map<Sk, Sv> map, Type<? extends Map<Sk, Sv>> type, Type<D> type1) {
                return null;
            }

            @Override
            public <Sk, Sv, D> List<D> mapAsList(Map<Sk, Sv> map, Type<? extends Map<Sk, Sv>> type, Type<D> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <Sk, Sv, D> Set<D> mapAsSet(Map<Sk, Sv> map, Type<? extends Map<Sk, Sv>> type, Type<D> type1) {
                return null;
            }

            @Override
            public <Sk, Sv, D> Set<D> mapAsSet(Map<Sk, Sv> map, Type<? extends Map<Sk, Sv>> type, Type<D> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <Sk, Sv, D> D[] mapAsArray(D[] ds, Map<Sk, Sv> map, Type<? extends Map<Sk, Sv>> type, Type<D> type1) {
                return null;
            }

            @Override
            public <Sk, Sv, D> D[] mapAsArray(D[] ds, Map<Sk, Sv> map, Type<? extends Map<Sk, Sv>> type, Type<D> type1, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> D newObject(S s, Type<? extends D> type, MappingContext mappingContext) {
                return null;
            }

            @Override
            public <S, D> MappingStrategy resolveMappingStrategy(S s, java.lang.reflect.Type type, java.lang.reflect.Type type1, boolean b, MappingContext mappingContext) {
                return null;
            }

            @Override
            public void factoryModified(MapperFactory mapperFactory) {

            }
        };
    }

}
