package com.app.evehiclerent.controllers;

import com.app.evehiclerent.dto.*;
import com.app.evehiclerent.service.PaymentService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping("/payment")
    public CreatePaymentResponseDto createPayment(CreatePaymentRequestDto createPaymentRequestDto){

        return paymentService.createPayment(createPaymentRequestDto);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping("/payment/{paymentId}")
    public FindPaymentResponseDto findPaymentById(@PathVariable @NotNull Long paymentId){

        return paymentService.findPaymentById(paymentId);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @PatchMapping("/payment/{paymentId}")
    public UpdatePaymentResponseDto updatePaymentById(@PathVariable @NotNull Long paymentId, UpdatePaymentRequestDto updatePaymentRequestDto){

        return paymentService.updatePaymentById(paymentId, updatePaymentRequestDto);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @DeleteMapping("/payment/{paymentId}")
    public void deletePaymentById(@PathVariable @NotNull Long paymentId){

        paymentService.deletePayment(paymentId);
    }


}
