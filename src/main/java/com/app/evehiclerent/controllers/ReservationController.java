package com.app.evehiclerent.controllers;

import com.app.evehiclerent.dto.*;
import com.app.evehiclerent.service.ReservationService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
public class ReservationController {

    @Autowired
    ReservationService reservationService;

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping("/reservation")
    public CreateReservationResponseDto createReservation(@RequestBody @Validated CreateReservationRequestDto createReservationRequestDto){

        return reservationService.createReservation(createReservationRequestDto);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping("/reservation/{reservationId}")
    public FindReservationByIdResponseDto findReservationById(@PathVariable @NotNull Long reservationId){

        return reservationService.findReservationById(reservationId);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @PatchMapping("/reservation/{reservationId}")
    public UpdateReservationResponseDto updateReservationById(@PathVariable @NotNull Long reservationId, @RequestBody @Validated UpdateReservationRequestDto updateReservationRequestDto){

        return reservationService.updateReservationById(reservationId, updateReservationRequestDto);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @DeleteMapping("/reservation/{reservationId}")
    public void deleteReservationById(@PathVariable @NotNull Long reservationId){

       reservationService.deleteReservationById(reservationId);
    }


}
