package com.app.evehiclerent.controllers;

import com.app.evehiclerent.dto.*;
import com.app.evehiclerent.service.UserService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController extends Evehiclerentcontroller {

    @Autowired
    UserService userService;

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping("/user/register")
    public RegisterUserResponseDto registerUser(@RequestBody @Validated RegisterUserRequestDto registerUserRequestDto){

        return userService.registerUser(registerUserRequestDto);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping("/user/{userId}")
    public FindUserResponseDto getUserById(@PathVariable @NotNull Long userId){

        return userService.findUser(userId);
    }

    @ResponseStatus(HttpStatus.OK)
    @PatchMapping("/user/{userId}")
    public UpdateUserResponseDto updateUserById(@PathVariable @NotNull Long userId, @RequestBody UpdateUserRequestDto updateUserRequestDto){

        return userService.updateUser(userId, updateUserRequestDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/user/{userId}")
    public DeleteUserResponseDto deleteUserById(@PathVariable @NotNull Long userId){

        return userService.deleteUser(userId);
    }



}
