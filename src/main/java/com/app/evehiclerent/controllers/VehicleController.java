package com.app.evehiclerent.controllers;

import com.app.evehiclerent.dto.*;
import com.app.evehiclerent.service.VehicleService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
public class VehicleController {

    @Autowired
    VehicleService vehicleService;

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping("/vehicle")
    public RegisterVehicleResponseDto registerVehicle(@RequestBody @Validated RegisterVehicleRequestDto registerVehicleRequestDto){

        return vehicleService.registerVehicle(registerVehicleRequestDto);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping("/vehicle/{vehicleId}")
    public FindVehicleResponseDto findVehicleById(@PathVariable @NotNull Long vehicleId){

        return vehicleService.findVehicleById(vehicleId);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @PatchMapping("/vehicle/{vehicleId}")
    public FindVehicleResponseDto updateVehicleById(@PathVariable @NotNull Long vehicleId, @RequestBody UpdateVehicleRequestDto updateVehicleRequestDto){

        return vehicleService.updateVehicleById(vehicleId, updateVehicleRequestDto);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @DeleteMapping("/vehicle/{vehicleId}")
    public DeleteVehicleResponseDto deleteVehicleById(@PathVariable @NotNull Long vehicleId){

        return vehicleService.deleteVehicleById(vehicleId);
    }
}
