package com.app.evehiclerent.dto;

import com.app.evehiclerent.entity.Payment;
import com.app.evehiclerent.entity.RentUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatePaymentResponseDto {

    private long id;

    private RentUser user;

    private long accountSource;

    private long accountDestination;

    private BigDecimal amount;

    private Payment.paymentStatus paymentStatus;

    private Payment.paymentDirection paymentDirection;

    public enum paymentStatus{
        SUCCESS,
        PROCESSING,
        FAILED
    }

    public enum paymentDirection{
        CREDIT,
        DEBIT
    }

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

}
