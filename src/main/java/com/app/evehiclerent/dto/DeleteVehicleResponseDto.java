package com.app.evehiclerent.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeleteVehicleResponseDto {

    private String name;

    private String brand;

    private String colour;

    private vehicleType vehicleTypes;

    private int year;

    private String registrationPlateNumber;

    public enum vehicleType{
        CAR,
        BIKE,
        BICYCLE,
        BOARD
    }

}
