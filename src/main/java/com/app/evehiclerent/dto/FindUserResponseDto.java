package com.app.evehiclerent.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FindUserResponseDto {

    private String name;

    private String email;

    private String address;

    private String password;

    private String phoneNumber;
}
