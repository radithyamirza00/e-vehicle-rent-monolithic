package com.app.evehiclerent.dto;

import com.app.evehiclerent.entity.RentUser;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUserRequestDto {

    @NotNull
    private String name;

    @NotNull
    private String email;

    @NotNull
    private String phoneNumber;

    @NotNull
    private String password;

    private String address;

    private String gender;

    private RentUser.userType userTypes;

    public enum userType{
        ADMIN,
        USER,
    }

    private LocalDateTime birthDate;
}
