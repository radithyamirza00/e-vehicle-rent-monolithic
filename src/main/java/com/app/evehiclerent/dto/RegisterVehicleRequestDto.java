package com.app.evehiclerent.dto;

import com.app.evehiclerent.entity.Vehicle;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterVehicleRequestDto {

    private String name;

    private String brand;

    private String colour;

    private Vehicle.vehicleType vehicleTypes;

    private int year;

    private String registrationPlateNumber;

    private BigDecimal rentPrice;

    private String location;

    public enum vehicleType{
        CAR,
        BIKE,
        BICYCLE,
        BOARD
    }

}
