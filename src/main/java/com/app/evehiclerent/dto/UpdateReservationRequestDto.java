package com.app.evehiclerent.dto;

import com.app.evehiclerent.entity.RentUser;
import com.app.evehiclerent.entity.Vehicle;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateReservationRequestDto {

    private RentUser rentingUser;

    private Vehicle vehicle;

    private boolean isExpired;

    private LocalDateTime rentUntil;


}
