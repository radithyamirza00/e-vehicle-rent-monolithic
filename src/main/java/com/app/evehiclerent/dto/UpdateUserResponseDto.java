package com.app.evehiclerent.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUserResponseDto {

    private String name;

    private String email;

    private String address;

    private String password;

    private String phoneNumber;

    private LocalDateTime birthDate;

}
