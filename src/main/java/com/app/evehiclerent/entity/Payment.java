package com.app.evehiclerent.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(targetEntity = RentUser.class, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private RentUser user;

    private long accountSource;

    private long accountDestination;

    private BigDecimal amount;

    private paymentStatus paymentStatus;

    private paymentDirection paymentDirection;

    public enum paymentStatus{
        SUCCESS,
        PROCESSING,
        FAILED
    }

    public enum paymentDirection{
        CREDIT,
        DEBIT
    }

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;
}
