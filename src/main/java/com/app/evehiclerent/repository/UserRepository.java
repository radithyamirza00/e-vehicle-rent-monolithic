package com.app.evehiclerent.repository;


import com.app.evehiclerent.entity.RentUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<RentUser, Long>, JpaSpecificationExecutor<RentUser> {
}
