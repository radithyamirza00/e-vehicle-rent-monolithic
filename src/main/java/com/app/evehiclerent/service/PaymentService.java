package com.app.evehiclerent.service;

import com.app.evehiclerent.dto.*;
import com.app.evehiclerent.entity.Payment;
import com.app.evehiclerent.repository.PaymentRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    MapperFacade mapperFacade;

    public CreatePaymentResponseDto createPayment(CreatePaymentRequestDto createPaymentRequestDto){

        Payment createPayment= mapperFacade.map(createPaymentRequestDto, Payment.class);

        paymentRepository.save(createPayment);

        return mapperFacade.map(createPayment, CreatePaymentResponseDto.class);
    }

    public UpdatePaymentResponseDto updatePaymentById(Long paymentId, UpdatePaymentRequestDto updatePaymentRequestDto){

        Optional<Payment> searchPayment = paymentRepository.findById(paymentId);

        if(searchPayment.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        Payment updatedPayment = searchPayment.get();
        mapperFacade.map(updatePaymentRequestDto, updatedPayment);
        paymentRepository.save(updatedPayment);

        return mapperFacade.map(updatedPayment, UpdatePaymentResponseDto.class);

    }

    public FindPaymentResponseDto findPaymentById(Long paymentId){

        Optional<Payment> searchPayment = paymentRepository.findById(paymentId);

        if(searchPayment.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return mapperFacade.map(searchPayment,FindPaymentResponseDto.class);
    }

    public void deletePayment(Long paymentId){

        Optional<Payment> deletePayment = paymentRepository.findById(paymentId);

        if(deletePayment.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        paymentRepository.delete(deletePayment.get());
    }

}
