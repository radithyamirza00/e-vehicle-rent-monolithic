package com.app.evehiclerent.service;

import com.app.evehiclerent.dto.*;
import com.app.evehiclerent.entity.Reservation;
import com.app.evehiclerent.repository.ReservationRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class ReservationService {

    @Autowired
    MapperFacade mapperFacade;

    @Autowired
    ReservationRepository reservationRepository;

    public CreateReservationResponseDto createReservation(CreateReservationRequestDto createReservationRequestDto){
        Reservation createReservation = mapperFacade.map(createReservationRequestDto, Reservation.class);

        reservationRepository.save(createReservation);

        return mapperFacade.map(createReservation, CreateReservationResponseDto.class);
    }

    public FindReservationByIdResponseDto findReservationById(Long reservationId){
        Optional<Reservation> searchReservation = reservationRepository.findById(reservationId);

        if(searchReservation.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return mapperFacade.map(searchReservation, FindReservationByIdResponseDto.class);
    }

    public UpdateReservationResponseDto updateReservationById(Long reservationId, UpdateReservationRequestDto updateReservationRequestDto){
        Optional<Reservation> searchReservation = reservationRepository.findById(reservationId);

        if(searchReservation.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        Reservation updatedReservation = searchReservation.get();

        mapperFacade.map(updateReservationRequestDto, updatedReservation);
        reservationRepository.save(updatedReservation);

        return mapperFacade.map(updatedReservation, UpdateReservationResponseDto.class);
    }

    public void deleteReservationById(Long reservationId){
        Optional<Reservation> searchReservation = reservationRepository.findById(reservationId);

        if(searchReservation.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        reservationRepository.save(searchReservation.get());
    }




}
