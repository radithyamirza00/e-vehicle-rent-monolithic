package com.app.evehiclerent.service;

import com.app.evehiclerent.dto.*;
import com.app.evehiclerent.entity.RentUser;
import com.app.evehiclerent.repository.UserRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Base64;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    MapperFacade mapperFacade;

    public RegisterUserResponseDto registerUser(RegisterUserRequestDto registerUserRequestDto){
        RentUser registerRentUser = RentUser.builder()
                .name(registerUserRequestDto.getName())
                .email(registerUserRequestDto.getEmail())
                .userTypes(registerUserRequestDto.getUserTypes())
                .password(Base64.getEncoder().encodeToString(registerUserRequestDto.getPassword().getBytes()))
                .address(registerUserRequestDto.getAddress())
                .birthDate(registerUserRequestDto.getBirthDate())
                .build();

        userRepository.save(registerRentUser);

        return mapperFacade.map(registerRentUser, RegisterUserResponseDto.class);
    }

    public FindUserResponseDto findUser(Long userId){

        Optional<RentUser> searchUser = userRepository.findById(userId);
        if(searchUser.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        FindUserResponseDto responseDto = mapperFacade.map(searchUser, FindUserResponseDto.class);
        return responseDto;
    }

    public UpdateUserResponseDto updateUser(Long userId, UpdateUserRequestDto updateUserRequestDto){
        Optional<RentUser> searchUser = userRepository.findById(userId);
        if(searchUser.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        RentUser updatedUser = mapperFacade.map(updateUserRequestDto, RentUser.class);
        userRepository.save(updatedUser);

        return mapperFacade.map(updatedUser, UpdateUserResponseDto.class);
    }

    public DeleteUserResponseDto deleteUser(Long userId){

        Optional<RentUser> searchUser = userRepository.findById(userId);
        if(searchUser.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        DeleteUserResponseDto response = mapperFacade.map(searchUser, DeleteUserResponseDto.class);
        userRepository.delete(searchUser.get());

        return response;
    }

}
