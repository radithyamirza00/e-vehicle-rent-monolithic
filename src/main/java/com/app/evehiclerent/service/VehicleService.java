package com.app.evehiclerent.service;

import com.app.evehiclerent.dto.*;
import com.app.evehiclerent.entity.Vehicle;
import com.app.evehiclerent.repository.VehicleRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class VehicleService {

    @Autowired
    MapperFacade mapperFacade;

    @Autowired
    VehicleRepository vehicleRepository;

    public RegisterVehicleResponseDto registerVehicle(RegisterVehicleRequestDto registerVehicleRequestDto){

        Vehicle registerVehicle = mapperFacade.map(registerVehicleRequestDto, Vehicle.class);

        vehicleRepository.save(registerVehicle);
        return mapperFacade.map(registerVehicle, RegisterVehicleResponseDto.class);
    }

    public FindVehicleResponseDto findVehicleById(Long vehicleId){

        Optional<Vehicle> searchVehicle = vehicleRepository.findById(vehicleId);

        if(searchVehicle.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return mapperFacade.map(searchVehicle, FindVehicleResponseDto.class);
    }

    public FindVehicleResponseDto updateVehicleById(Long vehicleId, UpdateVehicleRequestDto updateVehicleRequestDto){
        Optional<Vehicle> searchVehicle = vehicleRepository.findById(vehicleId);

        if(searchVehicle.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        Vehicle updatedVehicle = searchVehicle.get();
        mapperFacade.map(updateVehicleRequestDto, updatedVehicle);


        vehicleRepository.save(updatedVehicle);

        return mapperFacade.map(updatedVehicle, FindVehicleResponseDto.class);
    }


    public DeleteVehicleResponseDto deleteVehicleById(Long vehicleId){

        Optional<Vehicle> searchVehicle = vehicleRepository.findById(vehicleId);

        if(searchVehicle.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        DeleteVehicleResponseDto response = mapperFacade.map(searchVehicle, DeleteVehicleResponseDto.class);
        vehicleRepository.delete(searchVehicle.get());

        return response;
    }

}
