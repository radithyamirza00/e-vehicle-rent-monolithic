DROP TABLE IF EXISTS public.payment;
DROP TABLE IF EXISTS public.reservation;
DROP TABLE IF EXISTS public.rent_user;
DROP TABLE IF EXISTS public.vehicle;

create table public.rent_user (
    id bigserial not null constraint rent_user_pkey primary key,
    name varchar(50),
    email varchar(255),
    address varchar(255),
    password varchar(255),
    birth_date varchar(50),
    user_types varchar(25),
    created_at timestamp,
    updated_at timestamp
);

create table public.vehicle (
    id bigserial not null constraint vehicle_pkey primary key,
    name varchar(50),
    brand varchar(50),
    colour varchar(25),
    vehicle_type varchar(255),
    "year" numeric(20, 4),
    registration_plate_number varchar(25),
    rent_price numeric(20, 4),
    location varchar(50),
    is_available boolean not null default true,
    created_at timestamp,
    updated_at timestamp
);

create table public.payment (
    id bigserial not null constraint payment_pkey primary key,
    user_id bigint not null references public.rent_user(id),
    account_source bigint,
    account_destination bigint,
    amount numeric(20, 4),
    payment_status varchar(25),
    payment_direction varchar(25),
    created_at timestamp,
    updated_at timestamp
);

create table "public".reservation (
    id bigserial not null constraint reservation_pkey primary key,
    user_id bigint not null references public.rent_user(id),
    vehicle_id bigint not null references "public".vehicle(id),
    is_expired boolean not null default false,
    rent_until timestamp,
    created_at timestamp,
    updated_at timestamp
);